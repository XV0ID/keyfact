/* Copyright 2017 Sira Pornsiriprasert */

package com.garngy.Keyfact;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class Console extends KeyAdapter {

	public static void main(String[] args) {
		JPanel panel = new JPanel();
		JLabel str = new JLabel(
				"<html><h2>Keyfact</h2><h><br>Version: 001a - Release<br>Created by: XV0ID<br>Usage: Press any key to get its information</h><html>");
		JFrame frame = new JFrame();
		frame.setTitle("Keyfact");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(660, 300);
		frame.getContentPane().setBackground(new Color(50, 50, 50));
		frame.getContentPane().setFocusable(true);
		frame.getContentPane().addKeyListener(new Console());
		frame.add(panel);
		panel.add(str);
		frame.setVisible(true);
	}

	public void keyReleased(KeyEvent e) {
		Component parent = e.getComponent();
		char c = e.getKeyChar();
		char up = Character.toUpperCase(c);
		String text = KeyEvent.getKeyText(e.getKeyCode());
		String message = String.format(
				"Name: %c, %s\nKeycode: %d\n Ascii (Binary): %s, %s\nAscii (Hex): %s, %s\nAscii (Octal): %s, %s\nAscii (Decimal): %d, %d\nUnicode (Name): %s, %s\nUnicode (Decimal): %s, %s\nUnicode (Hex): %s, %s",
				c, text, e.getKeyCode(), Integer.toBinaryString(c), Integer.toBinaryString(up), Integer.toHexString(c), Integer.toHexString(up), Integer.toOctalString(c), Integer.toOctalString(up), (int) c, (int) up,
				Character.getName(c), Character.getName(up), (int) c, (int) up,
				"\\u" + Integer.toHexString(c | 0x10000).substring(1),
				"\\u" + Integer.toHexString(up | 0x10000).substring(1));
		JOptionPane.showMessageDialog(parent, message);
	}

}